---
slug: protv3-beta
title: ProTV 3 Open Beta
authors: techanon
---

# ProTV 3 Beta

Today I'd like to introduce to you the next generation of ProTV: **Version 3.0** (Beta)

![salesman meme](salesman.png)

:::note

This beta version of ProTV is considered a stable-but-not-full release and some things _may_ change.  
Unless a _severe and critical_ issue is discovered, there should be NO **_BREAKING_** changes between this beta and the full release.
This version has been 10 months in development, many issues have been resolved, and in it's current state, it is considered ready for production use.

:::

## Changes

In this version, there are a plethora of changes and improvements to all aspects of the tool.  
The following is a non-exhaustive list of the most significant features and changes that are now available:

### **First-class RenderTexture Support**
- Everything related to the video rendering side of ProTV now goes through a RenderTexture via a custom Blit operation.
- This enables being able to very easily use _any_ shader with the TV by simply having the material reference the RenderTexture the TV is using.
- It can also be used for lighting utilities like LTCGI and AreaLit.
- The texture can also be exposed to avatars via the [Global Shader Variables](/api/shaders-avatars)!

### **Expanded Security System**
The internal security management has been overhauled to enable more flexibility and customization.  
- Some of these things include handling for:
    - Group instances
    - On-the-fly popup events
    - High-profile festivals 
    - And much more
- Controlling the lock state of the TV by authorized users is now much more reliable.
- ProTV now utilizes a plugin format for enabling custom authentication logic, including the new ManagedWhitelist plugin, that can be used for managing the TV during events or festivals.

### **Automated Build Script**
- Ensures certain guarantees and attempts to fix the most common issues and will run implicitly upon both entering playmode and Build & Test. (_Very notably the VRCUrlInputField issue!_).
- You can also manually execute the script via the menu option `Tools -> ProTV -> Update Scene`.
- It also comes with a preview window, so you can examine what will change in the scene before it does.
- This script is designed to be as minimally invasive as possible to avoid contamination with other utilities or tools.

### **Overhauled Prefabs**
This update has significantly reworked the available prefabs provided by default.
- The default prefabs have been reduced to a single theme, Monochrome, with a color variation for just the media controls.
- All prefabs now come with TextMeshPro text by default for better legibility!
- The original prefabs (such as Retro) have been moved into a separate community contributor package: [ArchiTech.ProTV.Extras](https://gitlab.com/techanon/protv-extras)

If you would like to contribute a ProTV theme, skin or plugin for the community to use, please make a PR to the above linked Extras repository.

### **Major Refactoring of All Scripts**
Previously there was a lot of technical debt that existed in ProTV from the early days of development. ProTV 3 has cleaned up a lot of that.
- The plugin event system is much more reliable and consistent. Event execution order is now much more predictable for plugin designers.
- Late-joiner sync reliability has improved by a lot. This includes when dealing with GameObject toggling, which Udon is notoriously poor at handing out-of-the-box.
- User-to-user time sync has been greatly improved as well as recovery from failure states.

Additionally, ProTV 3 is forward compatible with the upcoming release of AudioLink 1.0.0! 
This includes utilizing the new media API that has been added to that version, so ProTV 3 will work with it out of the box.

And there is many more smaller changes and Quality of Life improvements (we haven't even mentioned the changes to the inspectors yet!) that are included in this update.

## Already in use!
During the alpha phase of ProTV 3, we had a handful of skilled creators get their hands on the development version to test out implementing it into their worlds.
Thanks to their faith in this project, many bugs have been squashed and a plethora of UX improvements have been made based on their feedback.

A number of worlds have already put ProTV 3 into production use. Here are some notable ones:
- [StabbyCinema](https://vrchat.com/home/world/wrld_cc341f9d-9363-49c5-b7e6-262c1d0d8f45), a new high quality, immersive cinema experience.
- [MusicVket 5 LiberaTune](https://vrchat.com/home/world/wrld_c282a72c-c1d0-48db-81ba-29c8d7ad1732), an open live-performance world by the Vket team.
- [Movie & Chill](https://vrchat.com/home/world/wrld_791ebf58-54ce-4d3a-a0a0-39f10e1b20b2), a media hangout world with a wide variety of content.
- [PolyWorld](https://vrchat.com/home/world/wrld_ee8b1b4c-5e57-4394-a864-a02b370c8676), a low-fi megaworld with a 7.1 surround compatible cinema.
- [LSMedia](https://vrchat.com/home/world/wrld_1b68f7a8-8aea-4900-b7a2-3fc4139ac817), a cozy movie world with a large selection of content.
- [Transparency](https://vrchat.com/home/world/wrld_80925927-1902-4cdc-954e-7b9e50b69995), a pleasant hangout world that's great for moderate-sized gatherings and social events.
- [Etiquette](https://vrchat.com/home/world/wrld_7ca0c913-1378-4fab-9e98-5e2f6ed7af20), an interactive education experience about design theory.

## It Is Time
To begin using this new version of ProTV, please check out the [Getting Started](/start) page!

If you have been using ProTV 2.x and wish to upgrade, please visit the appropriate documentation pages: [Upgrading](/upgrade)

:::danger

During the 3.0 beta phase, you will need to enable the `Show Pre-Release Packages` option in VCC to see the beta releases.  
In VCC click on Settings -> Packages -> Show Pre-Release Packages. You will then be able to see the ProTV beta release.  
If you do not, ProTV beta will not show up properly.

:::

## Support the Project
If ProTV or its community has helped you in any way, please strongly consider helping this project out with a generous donation on [KoFi](https://ko-fi.com/architechvr), [Gumroad](https://gum.co/protv) or [Booth](https://architechvr.booth.pm/items/2536209). Thank you!