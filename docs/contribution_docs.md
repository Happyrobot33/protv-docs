---
slug: /contribution/docs
title: Docs Contribution
---

# Documentation Contribution Rules and Guidelines

<!--truncate-->

Contributions to the documentation are done through pull requests to the [ProTV Docs Gitlab Repository](https://gitlab.com/techanon/protv-docs)

## Organization

This repository is organized primarily by target audience followed by topics of focus.

The main two categories are split into one for Creators and another for Developers.  
The purpose of the former is any and all information related to the setup, configuration and operation of ProTV that does not involve creating scripts (or graphs) for custom logic.
The purpose of the later category is to contain information about the API and how to integrate with or extend functionality of ProTV's ecosystem, namely things that involve custom scripts (or graphs).

Each of the 'target audience' categories have separate sidebars and respectively are organized into sub-folders in the `docs` folder.  
The optional exception is for the `docs/legacy` folder as that is temporary and will eventually be unnecessary once the migration has completed.

The sidebar definition for each category is explicitly defined within the `sidebars.js` file in the root directory. Any sub category definitions shall be defined in there.  
No use of the `_category_.json` shall be permitted (with optional exception of the `legacy` content).

## Document File Definition

Each file (with optional exception of the `legacy` content) must adhere to the following naming and file header conventions:
- Each document filename shall be either markdown `md` or markdown extended `mdx` filetype. You can read more about the mdx file format on the [MDX Docs](https://mdxjs.com/docs/).
- Each document filename must only be lowercase and contain only letters, numbers, hyphens, periods or underscores. No additional special characters are allowed.
- Each document file must contain certain header content called `front matter`. This front matter is composed of three hyphens to signify the start and end of the header with metadata defined in yaml configuration format. The accepted values can be found on the [docusaurus website](https://docusaurus.io/docs/api/plugins/@docusaurus/plugin-content-docs#markdown-front-matter). 
- All front matter must contain the minimum of a `slug` field and `title` field. The slug field will define the URL path that the document is accessible at and the title will define the text that shows up in the sidebar links and website meta title.
- Example:
```yml
---
slug: /urls/parameters
title: Url Parameters
---
```
- The file name must match the sanitized value of the slug from the respective file front matter.
    - The sanitization rule is: Remove the leading slash `/` and convert all other slashes `/` to underscores `_`.
    - Example: `slug: /guides/my-guide` shall become the file name `guides_my-guide.md` (the file name extension may vary depending on the features being used for that page)

## Inline Media Storage

Visual media included in the repository for the purposes of embedding inline in documents (such as screenshots or demonstration animations/gifs) shall be stored within the `/static/media` folder.  
This folder is organized similarly to the document files and filenames. The only difference is that the end of the filename, prior to the file extension, shall contain some meaningful set of terms which describes what the image is about. Optionally the meaningful term can also be a incremental number if the media represents steps in a process.

Example: An inline image referenced in the `/guides/my-guide.md` file, which is part of the `creators` category, would be located at `/static/media/creators/guides_my-guide_image-description.png`