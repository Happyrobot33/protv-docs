---
slug: /guides/arealit
title: AreaLit
description: How to setup ProTV for AreaLit
---

# AreaLit Integration

:::caution

This guide assumes you have AreaLit already imported into the project.  
If you need help with AreaLit stuff outside of this guide, check the official documentation from their asset https://booth.pm/en/items/3661829

:::

Getting ProTV setup for AreaLit is pretty straight forward. Most of these instructions are just ported from the official documentation in the link above with ProTV specific info added.

# Step 1 - Add AreaLit to Scene
First you need to add the LightCam prefab into your scene. You can find it located at `Assets/AreaLit/Example/LightCam`.  
You can adjust the Size and Clipping Planes on the LightMeshCam as you need for your scene.

# Step 2 - Setting up the Screen
Next, create a new Material somewhere you will remember it, then update it to the `AreaLit/LightMesh` shader.  
Go to the mesh GameObject you are using as the screen (eg: MainScreen in the Simple TV prefab), add a material slot to the MeshRenderer and attach the newly created material into that slot.  
Then set the layer of that mesh to TransparentFX (which is the layer that the LightCam is configured for by default). Make sure that this screen is physically within the bounds of the LightMesh cameras, otherwise it won't be visible in the reflections.  
![Update Screen Materials](/media/creators/guides_arealit_01-update-screen.png)  

# Step 3 - Update the Affected Meshes
Next you need to update your meshes to an AreaLit supported shader. It is recommended to use the `AreaLit/Standard` shader if you want something that just works.  
If you need to, make a new material and set it to that shader, then assign it to the desired MeshRenderers.  
![Update Affected Meshes](/media/creators/guides_arealit_02-affected-meshes.png)

# Step 4 - Create and Connect the Video Texture
Finally, on the Materials for the affected meshes, you will need to connect the TV's render texture up to it.  
On the TVManager, click the `Create` button next to the RenderTexture Target field. This will bring up a file prompt for you to save the created RenderTexture in your assets folder and connect it up to the TV once saved. Choose a location you will remember.  
![Create Render Texture](/media/creators/guides_arealit_03-create-render-target.png)  

After you've made the new RenderTexture file, go to the materials for the affected meshes. Connect up the `LightMesh.rendertexture` to the `Mesh` slot, and the created TV RenderTexture up to the `Texture 0` slot.  
![Assign Textures](/media/creators/guides_arealit_04-assign-textures.png)


That's it. If you have the [VideoPlayerShim](https://gitlab.com/techanon/videoplayershim/-/releases) imported, you can enter play-mode to validate that everything is setup, otherwise run a build & test to check everything is good.
