---
slug: /guides/audio
title: Configuring Audio
---

# Configuring Audio

The ins and outs of what you need to know when customizing your audio experience with ProTV.

TBW

Bullet Points to write about:
- Basic of setting up a speaker
- ProTV options for managing audio
    - Positional speaker setup (spatial audio)
    - Headphones setup (global audio)
- Limitations of AVPro
    - no audio effects (lowpass/reverb)
    - poor linux support
    - no playback speed control
- Limitations of UnityVideo
    - no livestreams
    - Only supports one speaker for most content
- Limitations of WindowsMediaFoundation
    - Why WMF?
    - Codecs
- Channels vs Tracks
- Surround Sound considerations and configurations
    - Brief on different SS configurations
    - Discrepancies of codecs


