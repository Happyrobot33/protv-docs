---
slug: /guides/audiolink
title: AudioLink
description: How to setup ProTV with AudioLink
---

# AudioLink Integration

## Connecting the Pieces
ProTV provides a button on any TVManager component to setup the minimum connections for AudioLink to read data from ProTV's ecosystem. This button will add AudioLink into the scene and setup the requisite AudioAdapter plugin for the TV, which automatically manages the AudioSource reference that AudioLink reads from.  
![AudioLink Connect Button](/media/creators/guides_audiolink_first-button.png)  

:::note

AudioLink can only read from one AudioSource so generally you will only have one AudioAdapter in your scene.

:::

## Picking Your Speakers
On the newly added AudioAdapter object, you should see all the detected speakers listed. It defaults to selecting the first speaker found, but you can choose whichever speaker you want AudioLink to react to from those dropdowns. Each VideoManager associated with that TV will have it's own speaker selection available.    
![AudioAdapter Speaker Selection](/media/creators/guides_audiolink_speaker-selection.png)  
For any UnityVideo option, it will typically only have one speaker available due to certain audio limitations with the video player. So you'll generally just pick the AVPro speakers to use.  

### Global AudioLink Speaker Option
By default, the speakers are all 3d spatialized and have a taper off built into the audio sources. But if you want AudioLink to always react no matter where in the world the user is, you can make another speaker (duplicating an existing one is fine) and change the settings to the following:
- Remove any VRCSpatialAudioSource component
- Change the VRCAVProVideoSpeaker's `Mode` to `Stereo Mix`
- Uncheck the `Spatialize` option
- Set the `Spatial Blend` to `0` (2D).
- Set the `Stereo Pan` to `0` (balanced).
- Set the `Volume` to `0.0006` (in-audible but non-zero).
- If you think AudioLink seems to be too low or not reactive enough, increase this value to `0.001`. 
The audio will become very slightly audible, but will ensure a clean signal for AudioLink to parse for more quiet songs.
![Global AudioLink Speaker Settings](/media/creators/guides_audiolink_global-speaker-settings.png)  
Then go back to the AudioAdapter and update the selection to the new speaker(s) you just created for AudioLink use.