---
slug: /guides/ltcgi
title: LTCGI
description: How to setup ProTV for LTCGI
---

# LTCGI Integration

:::caution

This guide assumes you have LTCGI already imported into the project.  
If you need help with LTCGI stuff outside of this guide, check the official documentation at https://ltcgi.dev

:::

Getting ProTV setup for LTCGI is pretty straight forward. Most of these instructions are just ported from the official documentation in the link above with ProTV specific info added.

### Step 1 - Add LTCGI to Scene
You will need to add the LTCGI controller to the world. You can find this at `Packages/LTCGI/LTCGI Controller.prefab`.  
Drag into your scene and it will do some automated setup stuff. Once LTCGI has finished it's setup, you can then proceed.

### Step 2 - Create and Connect the Render Texture
:::note

This step can be automated with the button at the top of the TVManager.  
![Automatic Render Target Assignment](/media/creators/guides_ltcgi_00-automatic-render-target.png)

:::

Next you will need to enable manual RenderTexture on the TVManager. Scroll down to the `Rendering Options` section and click the `Create` button next to the RenderTexture Target slot.  
![Create Render Target](/media/creators/guides_ltcgi_01-create-render-target.png)  
This will bring up the file save dialog. Choose a location in your Assets folder to store the RenderTexture file. Once you save the file the TV will have it connected up.  
With the new RenderTexture, connect it into the Video Texture slot on the `LTCGI Controller` prefab in the scene.  
![Assign Render Target](/media/creators/guides_ltcgi_02-assign-render-target.png)

### Step 3 - Add the Screen
Next you need to choose which meshes are going to be the source of "light source" for the render texture. For specifics on limitations with realtime textures, please read the LTCGI documentation.  
For now we will assume you are using the Simple prefab for ProTV, which has a Quad as the screen. Go to that Main Screen game object and add the `LTCGI_Screen` component to it.  
With the screen added, update the `Color Mode` option to Texture. It should default to 0, which is the Live Video option.  
![Add LTCGI Screen](/media/creators/guides_ltcgi_03-add-screen-component.png)  

:::note

At time of writing, according to the LTCGI documentation, live video screens can ONLY be a triangle or quad (2 triangles).  
If you have a custom mesh you are using, you'll need to add a dummy quad mesh for LTCGI to simulate from.

:::

### Step 4 - Shaders
In order for LTCGI to work in your world, you will need to make sure that all the meshes you want the reflections to be put on are using materials with shaders that explicitly support LTCGI's functionality. There are multiple [community shaders](https://github.com/PiMaker/ltcgi#supported-shaders) available that have the features available with a toggle, but LTCGI does also provide some shaders with common functionality. You can find these in the `LTCGI` submenu of the shader selector dropdown for materials. If you don't know what to go with, the `LTCGI/Surface (APIv2)` is a good place to start.


That's it. If you have the [VideoPlayerShim](https://gitlab.com/techanon/videoplayershim/-/releases) imported, you can enter play-mode to validate that everything is setup, otherwise run a build & test to check everything is good.