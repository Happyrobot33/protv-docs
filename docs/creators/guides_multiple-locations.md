---
slug: /guides/multiple-locations
title: Handling Multiple Locations in a World
---


There are various ways to configure a media player asset to handle multiple locations with in a single world. This guide will cover the most common cases.

## Duplicating a "Room"

The simplest option is to directly duplicate game objects. The default ProTV prefabs have the concept of a `Room` built in. It is a game object which contains the 'screens' and 'speakers' of the TV. If you click on the `Room` and press CTRL+D, you will now have an extra set of speakers and screen that you can then move around the world as you see fit.

Once you duplicate, you will need to ensure the new speakers are connected up correctly or the audio controls won't work properly for them.  
If you are using the default `Simple (ProTV)` prefab, you will need to go into the duplicated room's speakers and delete the `UnityVideo Stereo` speaker. More info on why is available in the [Audio Guide](/guides/audio).

For the remaining AVPro speakers in the new Room, the respective AVPro Video Player's VPManager needs to know that you want it to manage the audio of these AudioSources.  
Go to one of the speakers in the room and find the Video Player reference. Click it to highlight the related game object. Then click on that game object to reveal the VPManager.  
You will notice some new unchecked speakers listed. Enable those checkboxes for the speakers you want the manager to control the audio of (ie volume and the like).

![Enabling New AVPro Speakers](/media/creators/guides_multiple-locations_vpmanager-speakers.png)