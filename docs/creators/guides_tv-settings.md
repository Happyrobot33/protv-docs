---
slug: /guides/tv-settings
title: TVManager Settings
---

## Autoplay Settings

- __`Autoplay Main URL`__  
  Pretty straight forward. This field is a place to put a media url that you wish to automatically start upon first load into the world. This only is true for the instance owner (aka master) upon first visit of a new instance (which is then synced to any late-joiners). Leave empty to not have any autoplay.

- __`Autoplay Alternate URL`__  
  This field is the complementary url field to the above one. Quest users default to using the alternate url, but will fall back to the main url if it's not present. Leave both this and the above field empty to not have any autoplay.

- __`Autoplay Title`__  
  If you want your autoplay urls to not display the domain name, you can specify a custom text to be made available to UIs instead.

## Default TV Settings

- __`Default Manager`__  
  This is a dropdown of the video managers from which to pick the manager that the TV starts off selected with.

- __`Default Volume`__  
  Pretty straight forward. This specifies what the volume should be for the TV at load time between 0% and 100% (represented as a 0.0 to 1.0 decimal)

- __`Start With 2D Audio`__  
  Flag to specify if the TV should initialize with stereo audio mode or full spatial audio mode.

## Sync Options

- __`Sync To Owner`__  
  This setting determines whether the TV will sync with the data that the owner delivers. Untick if you want the TV to be local only (like for a music player or something).

- __`Automatic Resync Interval`__  
  This option defines how many seconds to wait before attempting to resync. This affects two separate things.
    - First is the Audio/Video sync. This particularly affects AVPro for long form videos (like movies) where after some time or excessive CPU hitching (such as repeated alt-tabbing) can cause the audio to eventually become out-of-sync with the video (usually it's the video that falls behind).
    - Second is the Owner sync. This is where the owner of the current video determines the target timestamp that everyone should be at. ProTV has a loose owner sync mechanism where it will only try to enforce sync at critical points (like switching videos, the owner seeks to a new time in the video, owner pauses the video for everyone, etc).

  The default value of 300 seconds is usually good enough for most cases, and shouldn't need to be changed unless for a specific need.

- __`Play Drift Threshold`__  
  This value determines how far away from the tv owner that non-owners are allowed to drift (aka de-sync). When this delta threshold is exceeded, the non-owner will be forcefully seek'd to the owner's timestamp. If you don't want to have a forced resync threshold, set this value to `Infinity` (yes the actual word) to disable the automated enforcement.

- __`Pause Drift Threshold`__  
  This is more so a visual feedback feature where if the video is paused locally but the video owner is still playing, the video will update the current playback timestamp every some number of seconds determined by this option. If you don't want the video to resync while paused, set this value to `Infinity` (yes the actual word) to have it never update until the user resumes the video.
  One way to view it is as a live slideshow of what is currently playing. This is intended to allow people to see what is visible on the TV without actually having the media actively running. Can be useful for moderators to see what is playing without having the actual video running.

#### Sync Tweaks 

- __`Sync Manager Selection`__  
  This setting, when combined with `Sync To Owner` will restrict the video player swap mechanism to the owner, and sync the active video player selection to other users.

- __`Sync Volume Control`__  
  This setting, when combined with `Sync To Owner` will restrict the volume control to the owner, and sync the owner's volume to other users.

## Media Load Options

- __`Play Video After Load`__  
  This setting determines whether or not the media that has been loaded will automatically start to play, or require manually clicking the play button to start it. This affects all video loads, including any autoplay media.

- __`Buffer Delay After Load`__  
  If you wish to have the TV implicitly wait some period of time before allowing the media to play (eg: give the media extra time to buffer)  
  Note: There will always be a minimum of 0.5 seconds of buffer delay to ensure necessary continuity of the TV's internals.

- __`Max Allowed Loading Time`__  
  This specifies the maximum time that a media should be allowed to take to load. If this time is exceeded, the media error event will be triggered with a `VideoError.PlayerError`.

- __`Prefer Alternate Url For Quest`__
  This setting makes the quest/android platform prioritize the Alternate Urls over the Main Urls. If no alternate url is provided, it will gracefully fallback to the main url.
  If you wish to repurpose the dichotomy of the main/alt for whatever reason (such as a sub/dub or some other dichotomy), you will very likely want to disable this setting

## Security Options

- __`Auth Plugin`__  
  You may optionally provide an instance of a TVAuthPlugin to the TV for running security checks through. ProTV comes with a couple auth plugins that you can make use of: `TVUsernameWhitelist` and `TVManagedWhitelist`. If you want to learn about implementing your own authentication plugin, check out the [Auth Plugin Documentation](/api/auth-plugins)

- __`Allow Master Control`__  
  This is a setting that specifies whether or not the instance master is allowed to lock down the TV to master use only. This will prevent all other users from being able to tell the TV to play something else. This is enabled by default. NOTE: The instance _owner_ will always have access to take control no matter what.

- __`Locked By Default`__  
  This is a setting that specifies whether or not the TV is master locked from the start. This only affects when the first user joins the instance as master. The locked state is synced for non-owners/late-joiners after that point. This is disabled by default.

#### Security Tweaks

- __`Remember First Master`__  
  When this setting is enabled, the TV will remember the first user to join the instance. Combining this setting with the `Allow Master Control` option is helpful for group instances or public instances where there is no _Udon recognizable_ instance owner. (The VRChat UI may specify a group or world author as owning the instance, but _Udon itself_ does not acknowledge them as the actual instance owner). This is enabled by default

- __`First Master Super User`__  
  Combine this with `Remember First Master` to grant the first user in the instance Super User status. This is disabled by default.

- __`Instance Owner Super User`__  
  Determines whether the instance owner (aka instance creator of an invite or friends instance) should be treated as a Super user or just a regular authorized user. This is enable by default.

- __`Super User Lock Override`__  
  This setting determines whether a Super user can lock out regular authorized users from the TV. When this is enabled and a Super user activates the TV lock, all non-super users will be unable to activate the TV lock. This is effectively an "admin override" of sorts and can be helpful for when authorized users are doing undesired shenanigans with the TV.
  When this is disabled, super users can still take ownership of the TV by activating the TV lock, but it will not prevent other authorized users from taking ownership back.
  This is disabled by default.

- __`Disallow Unauthorized Users`__  
  When this setting is enabled, all non-authorized users will be unable to take control of the TV, even when the TV's lock state is unlocked. This setting is not necessary under some scenarios. Each world creator will need to determine the necessity of this option. This is disabled by default.

- __`Pause Takes Ownership`__  
  This option grants the Pause actions initiated by a user to immediately take ownership if they are authorized to do so. This effectively turns the Pause button into a "Global Pause" action where anyone (who meets the authorization requirements) can pause the video for anyone else by taking ownership and syncing the pause state to everyone else.

- __`Enable Auto-Ownership (Experimental)`__  
  TBW

#### Domain Whitelist

- __`Enable Domain Whitelist`__  
  A flag for whether to enforce the domain whitelist check. This is disabled by default.

- __`Domain Whitelist`__  
  This is a list of domains for urls which, when the prior flag is enabled, the TV will adhere to and will only allow playing urls from said domains.
  This is very similar to VRChat's canonical [URL Whitelist](https://creators.vrchat.com/worlds/udon/video-players/www-whitelist), except that it is handled on a per TV basis.
  The default list consists of VRChat's url whitelist domains plus a few additional ones. You can add and remove the entries as you see fit for each TV instance you have.  
  The domains are searched using an 'ends with' fuzzy check. This enables subdomain passthrough for urls. For example: `bandcamp.com` enables the passthrough of all subdomains of bandcamp like `stessie.bandcamp.com`, which is necessary due to bandcamp creating a subdomain for every artist on the site. Similarly `youtube.com` enables the passthrough of `www.youtube.com`.


## Error/Retry Options

- __`Default Retry Count`__  
  This is the number of times a URL should be implicitly reloaded if the media fails to load. If the URL specifies an explicit `retry=X` hash param value, it will be used instead.

- __`Repeating Retry Delay`__  
  This value specifies how long to wait between each retry when a URL fails to load. This only applies to multiple sequential failures after the first retry attempt.

- __`Retry Using Alternate Url`__  
  This flag makes the TV attempt to load the alternate (or main if on quest) URL if the main (or alternate if on quest) URL fails to load the first time. It will only attempt it if the other URL is present and different than the current one. Because it has graceful fallback logic, this feature is enabled by default.

## Rendering Options

- __`RenderTexture Target`__  
  A RenderTexture that will receive the current frame of the TV's active VPManager.
  Automatically uses ProTV's internal Blit material for handling the texture transfer operation.
  If an explicit RenderTexture object is not provided, one will be created implicitly during runtime.

#### Texture Update Settings

- __`Allow Texture Resizing`__  
  TBW

- __`Auto MipMap Texture`__  
  Flag for whether the RenderTexture should have mipmaps applied or not. This setting is only used when no explicit RenderTexture is provided. Enabled by default.

- __`Texture Aspect Ratio`__  
  The value to use as the aspect ratio. This is not a declaration of the ratio of the video content,
  but rather declaring what the expected ratio is needed to confirm to your mesh's expected physical shape or requirements.
  This information is passed into the shaders for them to make the ratio adjustments to the UV as needed.
  
- __`Enforce Aspect During Resize`__  
  TBW

- __`Enforce Aspect During Blit`__  
  TBW

#### Standby Texture Settings

#### Material Targets

- __`Material`__  
  This material will receive the currently rendered texture and the data matrix of the TV's internal state. This is compatible with any shader.

- __`Texture Property`__  
  This is used in conjunction with the Material Target to declare which texture variable of the shader you wish to assign the data to.
  By default it is `_VideoTex`, but for custom shaders you may want to use `_MainTex` or `_Emission` depending on the need.
  There is no need to worry about the texture flip correction as that is handled internally during the TV's Blit operation.

- __`Global Shader Variables`__  
  Also called GSV.    
  Once you have a blit texture assigned, you can click this button to enable this TV to write to the _UdonTV global shader variables.
  To disable this, either pick another TV instance to assign, or remove the blit texture reference.
  

## Misc Options

- __`Start Hidden`__  
  This toggle specifies that the active video manager should behave as if it was manually hidden. This primarily helps with music where you might not want the screen visible by default even though the media is playing.

- __`Start Disabled`__  
  This toggle makes it so the TV disables itself (via SetActive) after it has completed all initialization.

- __`Stop Media When Hidden`__
  This toggle makes the TV call the stop logic for the media when it has been hidden (suppressed but not disabled, it keeps syncing active)

- __`Stop Media When Disabled`__
  This toggle makes the TV call the stop logic for the media when the TV game object itself becomes disabled.