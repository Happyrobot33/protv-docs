---
id: start
slug: /start
sidebar_position: 1
---

# Getting Started

Setting up **ProTV in less than 10 minutes**. This tutorial assumes basic
familiarity with Unity terminology.

:::caution

IF YOU ARE MIGRATING FROM 2.3.x, PLEASE REFER TO THE UPGRADE DOCUMENTATION HERE:
[Upgrading to 3.x](/upgrade/3.0.x)

:::

## What you'll need

### VCC Installation (Recommended):

:::danger

During the 3.0 beta phase, you will need to enable the
`Show Pre-Release Packages` option in VCC to see the beta releases.\
In VCC click on Settings -> Packages -> Show Pre-Release Packages. You will then
be able to see the ProTV beta release.\
If you do not, ProTV beta will not show up properly.

:::

- Go to [ArchiTech.Listing](https://vpm.techanon.dev) and click the top _Add to
  VCC_ button.
- Click on Manage Project for your project entry and add the latest version of
  ArchiTech.ProTV to your project.
- The dependency ArchiTech.SDK will be installed automatically when installing
  ProTV with VCC.

:::caution

If there is an open beta of the VRC SDK active, when adding a pre-release
package of ProTV (such as a beta release), it will attempt to update the SDK
packages as well to the beta. if this is undesirable, please ensure to downgrade
the `VRChat SDK - Worlds` package first, then the `VRChat SDK - Base` package
second.

:::

### Manual Installation:

:::danger

DO NOT INSTALL PROTV 3 INTO THE ASSETS FOLDER. THIS WILL BREAK THINGS AND PROTV
WILL NOT WORK PROPERLY.

:::

- Get the ProTV VPM formatted package from
  [here](https://gitlab.com/techanon/protv/-/releases) (grab the zip from the
  link under the Packages heading)
- Get the ArchiTech.SDK dependency package from
  [here](https://gitlab.com/techanon/architech-sdk/-/releases)
- Make sure you grab a compatible version of the dependency. You can double
  check the version number
  [here](https://gitlab.com/techanon/protv/-/blob/3.0.0-dev/package.json?ref_type=heads#L11)
- Extract the zip files into directories that you'll remember.
- In your unity project, open up the package manager from the menu
  `Window->Package Manager` and make sure to set the top-left dropdown to
  `In Project`.
- For both of the extracted packages (make sure to import the ArchiTech.SDK
  first), click the `+` button in the package manager and select
  `Add package from disk...`.
- Navigate to where you extracted the zip and select the package.json file in
  that folder.

## Add a prefab to the scene:

- You can find all pre-made prefabs available in the
  `Packages/ArchiTech.ProTV/Samples/Prefabs` folder.
- The prefab `Simple (ProTV)` is a basic TV setup that will work as a starting
  point for most worlds.
- Alternatively, you can also use one of the prefabs in the TVs subfolder (for
  example the `Live Events (ProTV)` prefab is great for clubs or concert
  worlds).
- Drag your prefab of choice into the scene window and modify it as needed.
- If you wish to give additional credits in the world to this asset, please make
  the credits out to `ArchiTechVR`. For historical reasons, you might also see
  it credited to `ArchiTechAnon`.

That's it! You have successfully added ProTV to your world! Next we'll look at
common ways to customize the TV.
