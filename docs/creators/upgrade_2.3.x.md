---
slug: /upgrade/2.3.x
sidebar_position: 1
title: 2.2 -> 2.3
---
# Upgrading from v2.2 to v2.3

This document contains the process and considerations for having a smooth transition between 2.2 and 2.3.x versions.

Due to various prefab structure changes and new stuff, it is highly recommended to do a clean import of ProTV (delete any pre-existing verison) for 2.3.

If you must do an upgrade, be sure to consider the following:
- Click on all playlists so that they show in the inspector. This will auto-upgrade the stored data on the object. Reimport the playlist file if there is one being used. Save the scene.
- Delete any existing Queue and ProTV Composite copies in scene and re-add a fresh copy as various bits of the structure changed in both. NOTE: ProTV Composite has been renamed to ProTV Hangout.
- Upgrade any AudioLink usage to 0.2.8
- If you unpacked ANY protv prefabs (or prefabs of any plugins) you should make sure to revisit those and see if they need updated or references reconnected. If something is broken with something you've unpacked, you will probably need to get a fresh prefab copy and update it accordingly as needed, or manually reconnect the references.
