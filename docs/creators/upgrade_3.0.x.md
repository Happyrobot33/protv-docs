---
slug: /upgrade/3.0.x
sidebar_position: 2
title: 2.3 -> 3.0
---
# Upgrading from v2.3 to v3.0

This document contains the process and considerations for having a smooth transition between 2.3.x and 3.0.0 versions.

:::danger

During the 3.0 beta phase, you will need to enable the `Show Pre-Release Packages` option in VCC to see the beta releases.  
In VCC click on Settings -> Packages -> Show Pre-Release Packages. You will then be able to see the ProTV beta release.  
If you do not, ProTV beta will not show up properly.

:::

:::caution

If you are upgrading from 2.2, please first refer to the upgrade documentation here: [Upgrading to 2.3](/upgrade/2.3.x)

:::

The upgrade process might not be perfectly smooth.
- These steps handle the generally most common setups people do in VRChat (probably ~90% of use-cases).
- If you did some special setup stuff, you may need to consider additional steps to make everything clean and optimized.

## Follow These Steps
- Add the repo listing at https://vpm.techanon.dev to your VCC.
- MAKE SURE YOUR PROJECT HAS A BACKUP, as always.
- If needed, make sure your project has the upgraded UdonSharp 1.x that comes with VCC (this is done implicitly when migrating a pre-VCC project)
- Ensure there are no existing compiler errors in your project before upgrading ProTV. If there are, start investigating the causes one-by-one from the top of the errors list.
- In VCC, add the latest version of ProTV 3.x to the project and let run the upgrade process.

:::caution

If there is an open beta of the VRC SDK active, when adding a pre-release package of ProTV (such as a beta release), it will attempt to update the SDK packages as well to the beta.
if this is undesirable, please ensure to downgrade the `VRChat SDK - Worlds` package first, then the `VRChat SDK - Base` package second.

:::

- After the upgrade process is complete, return to Unity (or open the project if it's not).
- In the top menu bar, select the `VRChat SDK -> Reload SDK` menu action.
- In the top menu bar, select the `Window -> TextMeshPro -> Import TMP Essential Resources` menu action to make sure TMP has been imported.
- In the top menu bar, select the `Tools -> ProTV -> Update Scene` menu action to do an initial pass on any ProTV corrections that may be needed.

:::note Preview the Build Checks

Alternatively you can select the `Tools -> ProTV -> Open Build Checks Window` menu action to view the logs window for the ProTV Build Checks.  
This will open a new editor window panel with "Dry Run" selected. With DryRun selected, it will display all expected changes to the scene that the build checks will make.  
You can use this information to plan ahead if needed. Otherwise you can untick DryRun and the click the "Run Build Checks" button to have those changes applied.  
Note: When applying the changes, the log will still contain the list of changes, since they were just applied. But if you run it again, the log should be cleared.

:::

- If you have any ProTV specific prefabs in the scene, you may need to go to them and ensure that all the references are still connected properly.
  - If there are a lot of unexpected missing references, you can generally right click the component header and select `Modified Component -> Revert` to restore the prefab defaults.
  - If you unpacked the prefabs, you may need to reconnect the TV's screen material to the TV's custom material slot.
- If any of the text is unexpectedly missing, you will need to enter then exit Unity's Play mode.
- If any of the text looks block-y or square, you will likely need to reset the z scale of the UIs.
  - Go to each affected Canvas component that has broken square text under it, right click and select the `ProTV -> Utility -> Reset Children Z Scale`.
  - This will go through each child transform and find any incorrectly scaled Z values that are set to 0, and update to an appropriate value.
  - This particular fix is mainly for TextMeshPro shader requirements.
- If you have AudioLink, you may want to double check the `AudioAdapter` in your scene (if you are using it) to ensure the correct speakers are assigned.
- If you cannot hear anything or the volume controls don't seem to be working, go to each of the `VPManagers` (usually as child objects of `Internal` game object)
  and ensure that the speakers are assigned as expected.
- If you are using any of the old style MediaControls prefabs, you may need to adjust the UI layout a bit as they come included with a video player swap action now.
- If you have any playlists in your scene, you may need to run the `Update Scene` action on each playlist inspector to fix any text rendering issues.

## Optional extra steps for custom setups
- Double check that you don't have any extraneous `VRCAVProVideoScreen` components on your custom MeshRenderer GameObjects.
  - Search in your hierarchy for `t:VRCAVProVideoScreen` and remove that component from any of your meshes.
  - The _ONLY_ `t:VRCAVProVideoScreen`s that should be present are the ones explicitly on objects with a VPManager/VPManagerV2 script on it.
