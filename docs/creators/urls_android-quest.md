---
slug: /urls/android-quest
sidebar_position: 1
title: Android and Quest
---

# Android and Quest

All video players, generally speaking, work on Android/Quest. Just gonna get that answer out of the way.

What _doesn't_ always work on Quest is the _urls_ themselves. There are a few points to make regarding this:

:::note

VRChat has pushed an update recently which enables the YoutubeDL on the Android build. This does not invalidate the information below, but it makes it become optional instead of required.  
There are some stability issues known to exist in the Android build in relation to the YoutubeDL tool, so the following information still has use.

:::

1) The urls that people are most used to aren't actually urls for _media_ but for _websites_. Most commonly is Youtube, which is mostly in the form of `https://youtube.com/watch?v=VIDEOID`. This url is a website url, not a media url (aka direct link). Video players do not know how to read a website url, so they would normally fail.  
BUT! VRChat has a trick up its sleeve to handle this. They use a tool called [YouTubeDownLoad(YTDL)](https://github.com/yt-dlp/yt-dlp/releases/latest). What this does is fetches the content of the webpage url, and fishes around that content until it finds a url that is a valid media url, then spits that back to VRChat so the program can tell the desired video player (either UnityVideoPlayer or AVPro) to play the direct link.

2) ~~Due to technical limitations beyond the VRChat dev's reach, YTDL is unable to be used natively on the Quest platform. Only PC has access to this tool currently. This means that when a user tries to play a generic youtube video using the common short-form that you see in the browser URL bar... it's not going to work.~~ This is no longer correct as VRChat has enabled YTDL on the android build.

3) The first alternative option we have is using the YTDL tool externally to VRChat on a PC. What this does is gives you the direct media url which you can then enter into the video player which will then sync to Quest users and they'll able to see it at that point.  
**There are many drawbacks to this method.**
* First is that it requires an external computer to run the YTDL program in order to parse the website url into the longer-form media url.
* Second is that for pretty much all large-scale media hosting platforms (such as youtube/video/dailymotion/etc) embeds a number of various parameters within the long-form urls, one of which is an expiration timestamps. So once the expiration timestamp has passed, the URL is no longer valid an will no longer load the video. This means that it is not realistic to put the URLs into the world at build time, as they would eventually not work. This also makes it difficult to simply share streams as well because most of those urls expire after an hour or two.

4) The second alternative is to use a custom server external to VRChat that is exposed to the internet which runs the YTDL tool for the user and then returns the media url. This helps because each time the URL is fetched, it has a new timestamp in it, so it can work for extended periods of time. The difficult thing is that it does require server admin knowledge if you set it up on your own. 
* There are multiple services that do this publicly.
* There is an open source option called [Vroxy](https://github.com/techanon/vroxy) if you want to host your own. It even includes setup and update scripts in the repo for any Debian-based OS (like Ubuntu) to make setup a breeze.
* One thing to note about using a server proxy like the above two, is if there is too much suspicious traffic from the server's IP, youtube/twitch/vimeo/etc can just rate-limit or straight up block the server's IP causing url resolution to fail. Some services have mitigations in place for dealing with this via custom backend proxy handling.

3) The final alternative is much more involved as it involves hosting the videos yourself from a custom server. You'd basically use YTDL to grab the video itself, then put it on a server you have and have people access the URL directly to your server instead of the mainstream hosting platforms. This is an advanced method that the average world creator is likely not going to know how to do.

The easiest and most accessible option currently is to use one of the public services. Though if you are not shy of doing a bit of server stuff, deploying an instance of Vroxy will generally make the urls more reliable. If you wish to know what public services are available, there is a maintained list of know services in the ArchiTech Assets discord server linked below.

All this doesn't even go into the actual media content, like codecs and formats, which can also contribute to video failures. But that's for another time.