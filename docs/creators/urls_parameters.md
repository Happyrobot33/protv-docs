---
slug: /urls/parameters
sidebar_position: 2
title: Url Parameters
---

# Url Parameters in ProTV

Purpose and usage of special url parameters.

In order to control various aspects of the TV on a per-video basis, ProTV comes with a system for handling parameters from a given url.  

It does this by parsing both the query (the bit after the `?`) and fragment (the bit after the `#`) portion of the URL.
If you are familiar with url query parameters, the url params in the fragment (aka hash) of the url are structured in a similar way.  

Query parameters are key/value pairs, where the value is optional. The key and value are separated by a `=` and multiple key/value pairs are separated by a `&` symbol.  

Likewise, the hash parameters are also key/value pairs. The key and value are still separated by a `=` symbol, but for the hash params, multiple key/value pairs are separated by a `;` symbol.

So for example `https://youtube.com/watch?v=VIDEO_ID?t=60#loop;start=30;end=120`  
This url would slice the video to 30 seconds through 2 minutes, start playing at 1 minute in and enable infinite looping.

You'll notice that the query parameter `t` was included in the result. The params system supports both hash and query style params.  
_**If a hash and query parameter exist with the same key, the hash param value overrides the query param's value.**_  

The list of currently available param keys are:
* `start`
    * Defines what timestamp the TV should treat as the beginning of the media, measured in seconds.
    * A side effect of this param is that the MediaControls plugin will have the left edge of the seek slider be that timestamp and the media  duration will be calculated as having the time prior to the start time not being included.
    * In effect, this parameter makes the TV _truncate_ the media prior to the defined time.
* `end`
    * Similar to the `start` param, it defines what timestamp the TV should treat as the ending of the media, measured in seconds.
    * Again, it makes the TV _truncate_ the media, but after the defined time.
* `t`
    * Specifies what timestamp the media should start playing from.
    * Useful when you have a long media and you just want to start watching from a specific timestamp.
    * Compatible with youtube's url share format via backwards compatibility with query params.
* `loop`
    * Tells the TV to repeat the current media some number of times after the media reaches the end.
    * `0` or if the parameter is not present, no looping occurs.
    * `-1` or if the parameter is present without a value, looping occurs indefinitely.
    * For any positive number above `0`, the TV will loop the media that many times before moving on
    * If loop is not provided, but the media requested is less than 30 seconds, loop will be implicily set to `1`
* `retry`
    * Tells the TV to reload the media _upto_ a given number of times if the media errors out.
    * `0` or if the parameter is not present, no reloading occurs. The error flag `errorOccurred` is set and the TV moves on.
    * `-1` of if the parameter is present without a value, reloading will occur indefinitely. Useful for permanent livestream links.
    * For any positive number above `0`, the TV will reload the media on error that many times before setting `errorOccurred` and moving on.
* `live`
    * An optional param to flag a url explicitly as expected to be a stream of some kind.
    * No value is used for this param.
    * This doesn't have any internal use for handling media, but it does help with error messaging when the stream goes down or is offline.
    * When a url fails to load in the first place (eg: stream is offline), it sends Udon a generic InvalidURL error. When the live param is set, it will tell the TV and plugins to handle the error in a slightly different manner, such as producing context relevant error messaging to let the user know that the stream _actually is_ offline instead of it just being a bad url.
* `3d`
    * An optional param that is passed into any connected materials for telling the shader to render the video in one of the given 3d modes.
    * Available modes: `1` = side by side, `2` = side by side (swapped), `3` = over under, `4` = over under (swapped)
* `aspect`
    * This value will override the default texture aspect ratio for the URL it's provided on.
    * This is useful for when a video provides a certain ratio that isn't what it should be and you need to force it to be corrected.

* There are 6 params specific to the Skybox plugin: `Panoramic`, `Cubed`, `180`, `NoLayout`, `SideBySide` and `OverUnder`
    * These can be learned about in the Skybox README file.

To use these params looks something like this: `https://youtube.com/watch?v=r2jgQuOmO48#start=419;end=617`  
You'll notice the syntax: a `#` followed by the first param key, it's corresponding value, then the `;` delimiter and another parameter/value set.
ProTV also supports extration of param values via the url query with `?` and `&` usage.  
Another example: `rtspt://stream.vrcdn.live/live/vrcdn?live&retry`  
Here you see the `live` param to declare it's expected to be a live media and the `retry` param with no value specifying that it should continually recheck for this URL. This use case is common for things like clubs or cafes that want a specific url to always be checked for.

:::note RTSPT links

In order to use url parameters with an RTSPT link, you _must_ use the url query syntax as the url fragment (the formal name for the  `#` part of a url) is not part of the spec for that protocol and will cause undefined behaviour if used.

:::