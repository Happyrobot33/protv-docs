---
id: welcome
slug: /welcome
sidebar_position: 0
---

# Welcome to ProTV

The documentation provided here is new and still in progress. 
It will be continued to be worked on during the Beta phase of ProTV 3. 
Please bear with us as we continue to improve and add information to the pages.

As things change, information may be shifted around between pages to improve organization and structure.
If you have any questions about where to find certain information, please join the discord (link below) and ask our Help Desk team.