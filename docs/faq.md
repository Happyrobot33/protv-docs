---
title: ProTV Troubleshooting and FAQ
slug: /faq
---

# ProTV Troubleshooting and FAQ
This page will contain various common issues and related solutions for getting ProTV to work how you want it to.
This page will expand as time goes on.

---
### How do I view the logs?

**SOLUTION**: 
The logs for VRChat are located in the APPDATA folder on windows `%APPDATA%\\LocalLow\\VRChat\\VRChat\\*.log`. Sort by date modified to get the latest log file. 

Though the better solution is to make use of U#'s log reader tooling by first enabling the debug log and UDON log flags `--enable-debug-gui --enable-udon-debug-logging` (pics 1 & 2 for reference) and then enable the U# log reader in the unity editor under the `Edit->Project Settings` menu (pic 3 for reference).

![steam game settings](/media/faq_logs-steamproperties.png)
![vrchat quicklauncher settings](/media/faq_logs-vrcql.png)
![unity project settings](/media/faq_logs-unityprojectsettings.png)

---
### How do I make the TV's audio global and not spatialized?

**SOLUTION**:
On the speaker game object, remove the VRCSpacialAudioSource component. Uncheck spatialize on the AudioSource if it's there, then remove any keyframes from the curve if there are any for spacial blend (only required if the spatial blend value says 'controlled by curves'), then set the spatial blend slider to 2D.

---
### I'm getting errors about IsValid missing the Utilities namespace.

**SOLUTION**:
VRCSDK menu -> Reload SDK
If that doesn't work, keep doing it, restart Unity, mix the two. If you've done dozens of times and it's still not working, make a new project and migrate your assets.

Welcome to unity development.

---
### I keep getting an error about 'ProTVBuildHelpers' failing.

**SOLUTION**:
Ensure that all TVs you have in scene, have their VideoManagers list populated correctly.

---
### The media isn't playing on Quest

OR
### How do I get youtube/soundcloud/etc to work on Quest?

**SOLUTION**:
Read through this post for more information:
https://ask.vrchat.com/t/protv-by-architechanon-usage-guides-and-walkthroughs/7029/11

---
### I bought a world that came with ProTV, but nothing is working

**SOLUTION**:
The world was likely poorly exported and did not properly include all necessary dependencies, thus breaking references.
The first thing you can do is download a copy of ProTV from booth or gumroad, then import the asset into the project to add any missing files.
If that doesn't fix things, you'll probably need to just drag in a new copy of ProTV and get rid of the old one.

---
### I'm trying to load a massive playlist of 1000s of entries and unity keeps freezing/lagging anytime I try to manage the playlist data.

**SOLUTION**:
Add an empty child game object directly under the affected playlist and attach the `PlaylistData` component to it. This will make the script store the massive amount of data on the child object to avoid the excessive serialization/deserialization stuff that Unity does which causes said freezing/lag.

---
### I imported a playlist with a lot of entries. My world size is now much bigger. Is there a way to reduce the world-size without losing any entries?

**SOLUTION**:
In C#, strings/urls are encoded in 16bit character arrays. This means that each character in a URL is 2 bytes.
If you want to keep all the entries and reduce the world size, you will generally want to shorten the URL lengths.

Here's an example set of numbers:
Given 800k entries (an extreme example), if we remove 10 characters off of each url... the world size would be decreased by almost 16 MB.
`10 chars * 2 bytes * 800,000 entries = 16 million raw bytes removed / 1024 = 15625 KB / 1024 = 15.25 MB`

Same goes for alternate URLs/titles/tags. The more data you have on each entry the more the world size adds up. Simple math. You need to determine your choice of balance between world size and desired data available.

---
### I want my AudioLink to work throughout the world without the user hearing it all the time

Or
### How do I setup my audio to be 2d?

Or
### AudioLink stops responding when I get too far from the audio source.

**SOLUTION**:
The easiest way to do this is the following steps:
- (for AudioLink) Make a copy of one of the audio sources
- Remove the VRCSpacialAudioSource component
- Uncheck `spatialize` on the audio source if it's enabled
- Set the AudioSource Spatial Blend to 2D (aka value `0`)
- Set the volume to `0.001`
- Delete all volume curve points (the red line) that are present on the AudioSource curve
- Set VRCAVProVideoSpeaker `Mode` to `StereoMix`
- Set `Stereo Pan` on AudioSource to Center (aka value `0`)
- (if AudioLink) Add the AudioSource  to the Unmanaged Speakers list on the respective VideoManager
- (if not AudioLink) Uncheck the Auto Manage 3D Mode option on the respective VideoManager

---
### The TV works, but I can't input any URLs in the UI

And/Or
### The media controls prefabs are missing the Main URL object

And/Or
### The script on the MediaInput game object is missing

This is due to a Unity import bug related to the SDK itself (Thanks Unity).

:::note Build Script Note

This issue should be resolved 99% of the time by running the build script via menu `Tools -> ProTV -> Update Scene`.  
Try that first, and if that doesn't work, try the below steps.

:::

**SOLUTION**:
Remove ProTV from the assets **and** scene. Save/close/reopen the project. Go to the `VRChat SDK` unity menu and select `Reload SDK`. Save/close/reopen the project. Then reimport ProTV into the project.

If that doesn't work, try the following alternate sequence that some have found to work:
Remove ProTV from the scene. Save/close the project. Go to the project's assets folder in your file explorer and delete the ProTV folder manually. Reopen the project and go to the `VRChat SDK` unity menu and select `Reload SDK`. Save/close/reopen the project. Then reimport ProTV into the project.

If that still doesn't work, you'll need to manually reconnect all the references. (See pictures)
When the VRCUrlInputField breaks, certain references are lost. Below (pic 1) points out what you need to reconnect (certain prefabs may differ from the picture, same process is needed though). Yellow is the object references and green is the events you need to re-add. 
Once that's done, you'll then need to reconnect the VRCUrlInputField back up to the controls (pic 2).

![url input field fixes](/media/faq_urlinput-fieldfix.png)
![url input field fixes](/media/faq_urlinput-referencefix.png)

---
### The TV reference is being removed from the Playlist plugin component

**SOLUTION**:
Unpack the playlist prefab. Unity likes the break that prefab for ProTV 2.x. This will be solved in the next major version.

---
### My youtube links don't always work, it depends on where it's coming from

**REASON**: 
(This is _one_ of multiple reasons why a youtube link may fail to resolve part of the time)
Links that are generated through a youtube-dl service (eg. nextnex, thetechnolus, vroxy) are tied to a particular country/legal jurisdiction, but are generally transferable between regions, meaning that I can generate the link on a server in Germany and open it on a client in the US for example. But as soon as the video is region-locked in any country, the links are generally no longer region-transferable. So if you have a video that is only blocked in Russia, you might not be able to transfer the link from Germany to the US, causing a 403.

---
### The loading bar on the TV is getting stuck at the end. What's going on?

**REASON**:
The loading bar is technically just an animation. There's no way to know what the progress actually is for a url request. 

If you are in VRChat directly and it gets stuck at the end, that means either the script has crashed or it's taking a long time to resolve. I have observed many times where a youtube video is taking excessive time to load (more than 30 seconds), but eventually it does either load or fail which will then disable the fake animation.

If you are trying to play it in editor via ClientSim, that likely means that it's trying to load a url into a video player component that doesn't exist causing the script to crash. To avoid this you need to follow the instructions on the video player shim tool's release page: https://gitlab.com/techanon/videoplayershim/-/releases

---
### Is there a way to load in a playlist from an external hosting source like pastebin or github?

**ANSWER**:
URLs cannot be created dynamically during runtime. This includes from a JSON response. VRCUrls can only be created during world build-time or manually entered into a VRCUrlInputField by a user in-game.

---
### Is anyone having an issue where the VRCAVProVideoScreen component is just deleting itself on play mode for multi screen setups?

**ANSWER**:
For ProTV 3, that's intentional. The VRCAVProVideoScreen components should only be on the VPManager game objects. ProTV 3 uses a different rendering pattern than ProTV 2 or other video player assets. All you need to worry about is making sure you have the materials assigned correctly to your mesh and the TVManager object in the Rendering Options.  
Having multiple AVProVideoScreen components for a single AVProVideoPlayer is a performance hit, so we enforce removing extraneous copies of that component where appropriate.  
Any of the ones that get removed automatically, you can just remove manually to avoid the confusion.

### Some of my text in my scene is blocky or square. What's wrong?

**SOLUTION**:
If any of the text looks block-y or square, you will likely need to reset the z scale of the UIs.
- Go to each affected Canvas component that has broken square text under it, right click the respective GameObject and select the `ProTV -> Utility -> Reset Children Z Scale` menu action.
- This will go through each child transform and find any incorrectly scaled Z values that are set to 0, and update to an appropriate value.
- This particular fix is mainly for TextMeshPro shader requirements.