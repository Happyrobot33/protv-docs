# ProTV 3 API Surface Changes
Fields, Properties and Methods that have been modified from the 2.3 LTS version of ProTV.  
This file is categorized by context and will only contain changes to class names, variable names and method
signatures.  
No explanation of the variables or signatures will be listed. Please find those in their respective documentation or source code files.
Please refer to this document for knowing what to look for when upgrading any dependant scripts or plugins.

## Namespace

All ProTV scripts have been moved from the `ArchiTech` namespace into the project specific `ArchiTech.ProTV` namespace.

## Core Scripts

### `TVManagerV2.cs` -> `TVManager.cs`
#### Added
- `IN_TITLE`
- `isReady`
- `enforceDomainWhitelist`
- `LoadingMedia`
- `SeekPercent`

#### Changed
- `IN_URL` -> `IN_PCURL`
- `IN_ALT` -> `IN_QUESTURL`
- `IN_SUBSCRIBER` -> `IN_LISTENER`
- `IN_PRIORITY` changed to `sbyte` type
- `autoplayURL` -> `autoplayMainUrl`
- `autoplayURLAlt` -> `autoplayAltUrl`
- `stateSync` -> `syncState`
- `state` -> `stateOwner`
- `currentState` -> `state`
- `localLabel` -> `title`

### `TVManagerV2SyncData.cs` -> `TVManagerData.cs`
#### Added
- `_CheckDomainWhitelist()`

#### Changed
- `_RequestSync()` -> `_RequestData()`

#### Removed
- `_SetTV()`

### `VideoManagerV2.cs` ->`VPManager.cs`
#### Added

#### Changed
- `_ApplyStateTo(VideoManagerV2)` -> `_UpdateState()`

#### Removed

### `UIShapeFixes.cs`
- Removed entirely. Logic moved to build script.

## AudioAdapter
### `AudioLinkAdapter.cs` -> `AudioAdapter.cs`
#### Added
- `targetSpeakers`
- `allowAudioLinkControl`
- `worldMusicResumeDuringSilence`

#### Changed
- `worldMusic` -> `worldAudio`
- `worldMusicResumeDelay` -> `worldAudioResumeDelay`
- `worldMusicFadeInTime` -> `worldAudioFadeInTime`

#### Removed
- `speakerName`

## MediaControls
### `Controls_ActiveState.cs` -> `MediaControls.cs`
#### Added

#### Changed
- `_UpdateMainUrlInput()` -> `_UpdateUrlInput()`
- `_UpdateAltUrlInput()` -> `_UpdateUrlInput()`
- `_EndEditAltUrlInput()` -> `_EndEditUrlInput()`

#### Removed
- `_ChangeAltMedia()`

## Playlist

### `Playlist.cs`
#### Added
- `descriptions[]`

#### Changed
- `SWITCH_TO_INDEX` -> `IN_INDEX`
- `urls[]` -> `pcUrls[]`
- `alts[]` -> `questUrls[]`
- `CurrentEntryMainUrl` -> `CurrentEntryMainUrl`
- `CurrentEntryAltUrl` -> `CurrentEntryAlternateUrl`
- `CurrentEntryTags` -> NEW
- `_SwitchToDetected()` -> `_SwitchEntry()`
- `_Switch()` -> `_SwitchEntry()`
- `_Switch(int)` -> `_SwitchEntry(int)`

#### Removed


## Queue

### `Queue.cs`
#### Added
- `IN_INDEX`
- `loop`
- `_AddEntry(VRCUrl, VRCUrl, string, bool = false)`
- `_SwitchEntry(int)`
- `_RemoveEntry(int)`
- `_MatchEntry(int, bool)`

#### Changed
- `IN_URL` -> `IN_PCURL`
- `IN_ALT` -> `IN_QUESTURL`
- `_QueueMedia()` -> `_AddEntry()`
- `_Remove()` -> `_RemoveEntry()`
- `_Purge()` -> split into `_PurgeSelf()`/`_PurgeAll()`
- `_Next()` -> `_Skip()`
- `_MatchCurrentUrl(bool)` -> `_MatchCurrentEntry(bool)`
- `_CheckCurrentUrl(bool)` -> `_CheckCurrentEntry(bool)`
- `_CheckNextUrl(bool)` -> `_CheckNextEntry(bool)`

#### Removed


## Skybox

### `SkyboxSwapper.cs`
#### Added

#### Changed

#### Removed


## VoteSkip

### `VoteSkip.cs`
#### Added

#### Changed

#### Removed

### `VoteZone.cs`
#### Added

#### Changed

#### Removed




