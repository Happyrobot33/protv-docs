# Queue Plugin
This plugin is a list of videos (external media URLs) that users can add to in-game and it will play them in sequence.  

## The Quick And Easy
This plugin still has improvements planned. The current recommended usage is to just drag the prefab into the scene (unpacking not needed) and then connect the desired ProTV in to the Tv reference slot.

NOTE: There are known issues with references getting lost. If you see the object fields missing values in the inspector of this prefab, click the 3-dot menu on the udonbehaviour component, open the `Modified Component` submenu and select `Revert`. This should fix any missing references on this prefab.