// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'ProTV Documentation',
  
  tagline: 'A feature-rich, extensible and secure media player system for VRChat with 3+ years of active development.',
  favicon: 'favicon.ico',

  // Set the production url of your site here
  url: 'https://protv.dev',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'ArchiTechVR', // Usually your GitHub org/user name.
  projectName: 'protv.dev', // Usually your repo name.

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  trailingSlash: false,

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          routeBasePath: '/'
        },
        blog: {
          blogTitle: "News",
          blogDescription: "The latest news and updates about ProTV",
          routeBasePath: '/news',
          showReadingTime: true,
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'site/social-card.png',
      navbar: {
        title: 'ProTV',
        logo: {
          alt: 'ProTV Logo',
          src: 'site/android-chrome-192x192.png',
        },
        items: [
          {to: '/news', label: 'News', position: 'left'},
          {
            type: 'docSidebar',
            sidebarId: 'creatorSidebar',
            position: 'left',
            label: 'Creators',
          },
          {
            type: 'docSidebar',
            sidebarId: 'developerSidebar',
            position: 'left',
            label: 'Developers',
          },
          {to: '/faq', label: 'FAQ', position: 'left'},
          {to: '/changelog', label: 'Changelog', position: 'left'},
          {
            href: 'https://vpm.techanon.dev',
            label: 'VCC Listing',
            position: 'right',
          },
          {
            href: 'https://ko-fi.com/architechvr',
            label: 'Donate',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Getting Started',
                to: '/start',
              },
            ],
          },
          {
            title: 'Community',
            items: [
              {
                label: 'VRChat Group',
                href: 'https://vrc.group/PROTV.3090',
              },
              {
                label: 'Discord',
                href: 'https://discord.gg/kYAGEDUFDV',
              },
              {
                label: 'GitLab',
                href: 'https://gitlab.com/techanon/protv',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Docs Contribution',
                href: '/contribution/docs',
              },
              {
                label: 'VCC Listing',
                href: 'https://vpm.techanon.dev',
              },
              {
                label: 'Gumroad',
                href: 'https://gum.co/protv',
              },
              {
                label: 'Booth',
                href: 'https://architechvr.booth.pm/items/2536209',
              },
            ],
          },
        ],
        copyright: `Copyright © 2020-${new Date().getFullYear()} ArchiTechVR. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
