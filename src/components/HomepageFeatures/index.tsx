import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

type FeatureItem = {
  title: string;
  description: JSX.Element;
};

const FeatureList: FeatureItem[] = [
  {
    title: 'Easy to Use',
    description: (
      <>
        It has been designed from the ground up to be easily installed and
        get integrated into your world quickly. With Drag-n-Drop prefabs
        and an intuitive editor UI, using ProTV is a breeze.
      </>
    ),
  },
  {
    title: 'Robust yet Flexible',
    description: (
      <>
        ProTV does what it can to be as out-of-your-way as possible while
        still enabling an extensible ecosystem and configuration flexibility,
        through integrity checks on build, to a reactive plugin system for
        integrating your own scripts.
      </>
    ),
  },
  {
    title: 'Created for the Community',
    description: (
      <>
        The VRChat community is very important to us. The core of ProTV will always be free and open source.
        The culmination of where ProTV is today is only due to the efforts of many people, all of whom are very appreciated.
      </>
    ),
  },
];

function Feature({title, description}: FeatureItem) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures(): JSX.Element {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
